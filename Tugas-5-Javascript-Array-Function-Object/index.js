//Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var jumlah = (daftarHewan.length)
for(var urut = 1; urut <= jumlah ; urut ++)
{
  switch(urut)
  {
  case 1:   { console.log('1. Tokek'); break; }
  case 2:   { console.log('2. Komodo'); break; }
  case 3:   { console.log('3. Cicak'); break; }
  case 4:   { console.log('4. Ular'); break; }
  default:  { console.log('5. Buaya'); }
  }  
}

//Soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming"}
function introduce ()
{
console.log('Nama saya '+data.name+','+' umur saya '+data.age+','+' alamat saya di jalan '+data.address+','+' dan saya punya hobby yaitu '+data.hobby);
}
introduce();

//Soal 3
function cekKata(str) {
	var vokal = 'aeiouAEIOU';
	var count = 0;

	for (var d = 0; d < str.length; d++) {
		if (vokal.indexOf(str[d]) !== -1) {
			count++;
		}
	}
	return count;
}
console.log(cekKata('Muhammad'));
console.log(cekKata('Iqbal'));

//Soal 4
function hitung(nilai) {
    return nilai * 2 - 2
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8