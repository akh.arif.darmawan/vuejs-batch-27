//Soal 2
var readBooks = require('./callback.js')
var books =
[
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

var readBooksPromise = require('./promise.js');

function startReading(remainingTime, books, index)
{
let read = readBooksPromise(remainingTime, books[index])
read.then
(content => 
{
const nextBook = index + 1;
        if (nextBook < books.length)
{
startReading(content, books, nextBook);
}
})
}
startReading(10000, books, 0);