//Soal 1
const ukuran=(p, l) => 
({
Luas: p * l,
Keliling: (2 * p) + (2 * l)
});
let panjang = 3;
let lebar = 5;
console.log(ukuran(panjang , lebar));

//Soal 2
var newFunction = (firstName, lastName) => ({firstName: firstName, lastName: lastName});
console.log(newFunction ("William","Imoh"));

//Soal 3
var newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log(newObject)

//Soal 4
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let gabungWilayah = [...west,...east]
console.log(gabungWilayah)

//Soal 5
const planet = "earth" 
const view = "glass" 
const merge = 'Lorem '+`${view}`+' dolor sit amet, consectetur adipiscing elit, '+`${planet}`
//var before = 'Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit, ' + planet
console.log(merge)