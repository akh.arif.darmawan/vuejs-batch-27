//Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var pilihan1= pertama.substr(0,5);
var pilihan2= pertama.substr(12,7);
var pilihan3= kedua.substr(0,8);
var pilihan4= kedua.substr(8,10);
var rubah= pilihan4.toUpperCase();
var gabung = pilihan1.concat(pilihan2,pilihan3,rubah);

console.log(gabung)

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var kPint = parseInt(kataPertama);
var kKint = parseInt(kataKedua);
var kTint = parseInt(kataKetiga);
var kEint = parseInt(kataKeempat);
var aritmatik = (kPint/kKint)+(kTint+kEint)

console.log(aritmatik)

//Soal 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do by me! 
var kataKetiga = kalimat.substring(15,18); // do by me! 
var kataKeempat = kalimat.substring(19, 24); // do by me! 
var kataKelima = kalimat.substring(25, 31); // do by me!

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
